/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.android.tv.settings.device.input;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import androidx.preference.Preference;
import androidx.preference.SeekBarPreference;
import androidx.preference.PreferenceScreen;

import com.android.settingslib.core.AbstractPreferenceController;

public class MouseModeController extends AbstractPreferenceController implements
        Preference.OnPreferenceChangeListener {

    private static final String KEY_MOUSEMODE_SETTING = "input_mousemode";
    private static final String KEY_POINTER_SPEED = "pointer_speed";
    private static final String KEY_STEP_DISTANCE = "step_distance";
    private static final int POINTER_SPEED_MIN = 10;
    private static final int POINTER_SPEED_MAX = 100;
    private static final int STEP_DISTANCE_MIN = 5;
    private static final int STEP_DISTANCE_MAX = 50;
    private SeekBarPreference mPointerSpeed;
    private SeekBarPreference mStepDistance;
    private Context mContext;

    private BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("com.softwinner.tv.MOUSEMODE_UPDATE".equals(intent.getAction())) {
                updateState();
            }
        }
    };

    public MouseModeController(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public void displayPreference(PreferenceScreen screen) {
        if (isAvailable()) {
            setVisible(screen, getPreferenceKey(), true);
            mPointerSpeed = (SeekBarPreference) screen.findPreference(KEY_POINTER_SPEED);
            mPointerSpeed.setOnPreferenceChangeListener(this);
            mPointerSpeed.setMin(POINTER_SPEED_MIN);
            mPointerSpeed.setMax(POINTER_SPEED_MAX);
            mStepDistance = (SeekBarPreference) screen.findPreference(KEY_STEP_DISTANCE);
            mStepDistance.setOnPreferenceChangeListener(this);
            mStepDistance.setMin(STEP_DISTANCE_MIN);
            mStepDistance.setMax(STEP_DISTANCE_MAX);
            IntentFilter filter = new IntentFilter("com.softwinner.tv.MOUSEMODE_UPDATE");
            mContext.registerReceiver(mUpdateReceiver, filter);
        } else {
            setVisible(screen, getPreferenceKey(), false);
        }
    }

    public void onStop() {
        mContext.unregisterReceiver(mUpdateReceiver);
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public String getPreferenceKey() {
        return KEY_MOUSEMODE_SETTING;
    }

    private void updateState() {
        ContentResolver resolver = mContext.getContentResolver();
        if (mPointerSpeed != null) {
            int speed = Settings.System.getInt(resolver, Settings.System.TV_MOUSEMODE_POINTERSPEED, POINTER_SPEED_MAX / 2);
            mPointerSpeed.setValue(speed);
        }
        if (mStepDistance != null) {
            int distance = Settings.System.getInt(resolver, Settings.System.TV_MOUSEMODE_STEPDISTANCE, STEP_DISTANCE_MAX / 2);
            mStepDistance.setValue(distance);
        }
    }

    @Override
    public void updateState(Preference preference) {
       updateState();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        ContentResolver resolver = mContext.getContentResolver();
        if (mPointerSpeed == preference) {
            int speed = (int) newValue;
            Settings.System.putInt(resolver, Settings.System.TV_MOUSEMODE_POINTERSPEED, speed);
        } else if (mStepDistance == preference) {
            int distance = (int) newValue;
            Settings.System.putInt(resolver, Settings.System.TV_MOUSEMODE_STEPDISTANCE, distance);
        }
        return true;
    }
}
