/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.tv.settings.device.input;

import android.content.Context;
import android.os.Bundle;

import com.android.internal.logging.nano.MetricsProto;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.tv.settings.PreferenceControllerFragment;
import com.android.tv.settings.R;

import java.util.ArrayList;
import java.util.List;

/**
 * The "Display" screen in TV Settings.
 */
public class MouseModeFragment extends PreferenceControllerFragment {

    private MouseModeController mController;

    public static MouseModeFragment newInstance() {
        return new MouseModeFragment();
    }

    @Override
    protected int getPreferenceScreenResId() {
        return R.xml.mousemode;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        updatePreferenceStates();
    }

    @Override
    protected List<AbstractPreferenceController> onCreatePreferenceControllers(Context context) {
        final List<AbstractPreferenceController> controllers = new ArrayList<>();
        mController = new MouseModeController(context);
        controllers.add(mController);
        return controllers;
    }

    @Override
    public int getMetricsCategory() {
        return 192;
    }

    @Override
    public void onStop() {
        super.onStop();
        mController.onStop();
    }
}
