package com.android.deskclock.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.android.deskclock.data.DataModel;
import com.android.deskclock.provider.AlarmInstance;
/**
 * Created by huihuixu on 2019/7/17.
 */

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        final int alarmState = intent.getIntExtra(AlarmStateManager.ALARM_STATE_EXTRA, -1);
        final long instanceId = AlarmInstance.getId(intent.getData());
        Intent in = AlarmInstance.createIntent(context, AlarmService.class, instanceId);
        in.setAction(AlarmStateManager.CHANGE_STATE_ACTION);
        in.addCategory("ALARM_MANAGER");
        in.putExtra("intent.extra.alarm.global.id", DataModel.getDataModel().getGlobalIntentId());
        in.putExtra(AlarmStateManager.ALARM_STATE_EXTRA, alarmState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            context.startForegroundService(in);
        }
        else{
            context.startService(in);
        }
    }
}
